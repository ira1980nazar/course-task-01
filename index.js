const express = require('express')
const bodyParser = require('body-parser');
const PORT = process.env.PORT ?? 56201;
const app = express()
app.use(bodyParser.text({type: '*/*'}));

// Endpoint for calculating square of a number
app.post('/square', (req, res) => {
    const num = Number(req.body);
    console.log(req.body);
    if (isNaN(num)) {
        return res.status(400).send({error: 'Invalid number'});
    }
    const square = num * num;
    res.send({
        number : num,
        square : square
    });
});


// Endpoint for reversing a text
app.post('/reverse', (req, res) => {
    const text = req.body;
    if (!text || Object.entries(text).length === 0){
        return res.status(400).json({error: 'Invalid input'});
    }

    res.setHeader('content-type', 'text/plain');
    const reversedText = text.split('').reverse().join('');
    res.send(reversedText);
});

function isLeapYear(year){
        return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
    }

function checkDate(day, month, year){
    if(month === '2'){
        if(isLeapYear(year))
            return +day <= 29;
        else
            return +day <= 28;
    }
    if(month === '4' || month === '6' || month === '9' || month === '11'){
        return +day <= 30;
    }
    if(month === '1' || month === '3' || month === '5' || month === '7' || month === '8' || month === '10' || month === '12'){
        return +day <= 31;
    }



}
// const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];


app.get('/date/:year/:month/:day', (req, res) => {
    if (isNaN(req.params.year) || isNaN(req.params.month) || isNaN(req.params.day)){
        return res.status(400).json({error: 'Invalid input'});

    }
    const {year, month, day} = req.params;
    if(!checkDate(day, month, year)){
        return res.status(400).json({error: 'Invalid date'});
    }else {
        const date = new Date(year, month - 1, day);
        const currentDay = new Date();
        res.send({
            weekDay: date.toLocaleDateString('en-US',{ weekday : 'long' }),
            isLeapYear: isLeapYear(year),
            difference: Math.abs(Math.floor((currentDay.getTime() - date.getTime()) / (1000 * 3600 * 24)))
        })
    }
});


app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}`)
})